# Mapa Conceptual "Computadoras Electrónicas"
```plantuml
@startmindmap
*[#LightCoral] **COMPUTADORAS ELECTÓNICAS**  
	*[#BB8FCE] Computadoras de 1 proposito evolucionan \n gracias a la necesidad de automatización y computación.
		*_ en 
			*[#D2B4DE] 1944 IBM contruye Harvard Mark I.
				
				*_ contenia 
					*[#33B9AB] - 765 mil componenetes \n - 80 km de cable \n - 1 eje de 15 m \n - 1 motor de 5H \n - 3 mil relés
						*[#ABEBC6] **RELÉ**
							*_ componentes
								*[#B2BABB] -Bobina \n - Brazo de hierro  \n - 2 contactos
							*[#B89B8D] Generaba un campo magnetico,\n gracias a la electricidad aplicada en la bobina,\n podia cambiar de estado 50 veces \n por segundo. 
  				*[#B2BABB] Fue utilizada para realizar calculos \n del proyecto Manhattan.	
				*_ Podia realizar 
					*[#0BCFE7] - 3 sumas o restas por segundo.\n - 1 Multiplicacion en 6 segundos. \n - Division en 15 segundos. \n -  Operaciones complejas más de 1 minuto.

			*[#E59866] Septiembre de 1947 \n fue creado el termino Bug, por Greace Hopper,\n al sacar una polilla muerta de la Mark I.
			*[#E74C3C] 1904 John Fleminng desarrolla \n la Valvula Termoióninca.
				*_ compuesta por
					*[#CB4335] -Filamento \n -2 electrodos (anodo y catodo) \n - Bulbo de Cristal. 	
			*[#2980B9] 1906 Lee de Forest, mejora el \n diseño de la Valvula Termoiónica \n añadiendo un electrodo de control \n entre el anodo y el catodo.
				*[#21618C] Podia cambiar de estado miles de veces por segundo.
					*[#2471A3] Fuerón piezas fundamentales para la fabricación de Radio, Telefonos a larga distancia etc.
			*[#21618C] Diciembre de 1943, Tommy Flowers \n crea "Colosus Mark I"
				*[#B3B6B7] Instalada en Bletchley Park (Reino Unido) \n fue usada para decodificar comunicados Nazis.
					*_ fue
						*[#884EA0] Alan Turing quien decifra el codigo Enigma Nazi con ayuda de THE BOMBE.
				*[#633974] Es considerada la \n Primer Computadora Programable.
					*[#76448A] Programada mediante cables en un tablero.  
			*[#58D68D] 1946 John William Mauchly \n y J.Presper Eckert, crearón la ENIAC \n (Calculadora Inteligente Numerica Electronica)
				*[#0BCFE7] La primer computadora ELectronica \n Programable de Proposito General.
					*[#28B463] Realizaba 5 mil sumas y restas \n de 10 digitos por segundo.
			*[#33B9AB] 1947, en Bell Laboratories: \n -John Bardeen.\n -Walter Brattain \n -William Shockley
				*[#B2BABB] Crearon el Transistor, \n interrumpor electronico, \n compuesto de Silicio.

			*[#D4AC0D] 1957 Es creada la IBM 608, Primer \n computadora disponible comercialmente.
				*[#E74C3C] Basada en transistores.
				*_ Realizaba 
					*[#B89B8D] - 4 mil 500 sumas \n - 80 divisiones o mulltiplicaciones \n por segundo.  
@endmindmap
```
# Mapa Conceptual "Arquitectura Von Neumann y Arquitectura Harvard"
```plantuml
@startmindmap
*[#21618C] **Arquitectura de Computadoras** 
	*[#B89B8D] **Ley de Moore**
		*_ Establce que 
			*[#E74C3C] La velocidad del procesador de las\n computadoras se duplica cada año.
				*[#E74C3C] **Electronica**
					*[#B89B8D] - El numero de transistores totales por chip se duplica cada año.\n - El costo del chip permanece sin cambios. \n- Cada 18 meses se duplica la potencia de cálculo sin modificar el costo.   
				*[#B2BABB] **Performance**
					*[#B3B6B7] - Se incrementa la velocidad del procesador.\n - Se incrementa la capacidad de memoria. \ La velocidad de la memoria es menor a la velocidad del procesador.

	*[#B89B8D] **Funcionamiento**	
		*_ Antes
			*[#E74C3C] Sistemas Cableados.
			*[#E74C3C] Programación mediante Hardware.
			*[#E74C3C] Secuencia de funciones aritméticas-Lógicas.
		*_ Ahora
			*[#58D68D] Programación mediante Software.
			*[#58D68D] Secuencia de funciones aritméticas-lógicas.
			*[#58D68D] Interpreta Instruccions y señales de control.
	*[#B89B8D] **Arquitectura de Von Neumann**
		*[#33B9AB] Arquitectura Moderna
			*[#B2BABB] Descrita por John Von Neumann en 1945
				*_ Componentes
					*[#ABEBC6] - CPU (Unida Central de Procesamiento). \n -Memoria Principal. \n - Sistema de entrada y salida. \n - Sistema de Buses				
						*[#E59866] - Los datos de Lectura se almacenan en una misma memoria de lectura.
						*[#E59866] - Los contenidos de esta memoria se acceden indicando su posición sin importar su tipo.
						*[#E59866] - Ejecucion en secuencia (salvo que se indique lo contrario).
						*[#E59866] - Representacion Binaria.
				*_ Surge el termino
					*[#ABEBC6] **Programa Almacenado**
						*[#E59866] Por la separacion de la memoria y la CPU, \n  creando **Nuemann bottleneck** cuello \n de botella de Neumann.
			*[#B2BABB] **Interconexión**
				*[#ABEBC6] Todos los componentes se comunican a través de \n un sistema de buses.				
					*[#E59866] Bus de Datos.
					*[#E59866] Bus de Direcciones.
					*[#E59866] Bus de Control.
	*[#B89B8D] **Arquitectura de Harvard**
		*[#33B9AB] Hace referencia a las arquitecturas que \n utilizaban dispositivos de almacenamiento separados \n uno para instrucciones y otro para datos.
			*_ Componentes	
				*[#ABEBC6] - Procesador o CPU. 
					*[#E59866] Unidad de control. \n Unidad Aritmética Lógica.
				*[#ABEBC6] - Memoria
					*[#E59866] Instrucciones
						*[#0BCFE7] Almacena instrucciones del programa utilizando diferentes tipos de memorias (ROM, PROM, EPROM o flash). 
					*[#E59866] Datos
						*[#0BCFE7] Almacena los datos utilizados por los programas, habitualmente usando memoria\nSRAM, o si es necesario almacenar datos permanentemente, EEPROM o flash.
					*[#E59866] Sistema de Entrada/Salida.
					*[#E59866] Buses para cada memoria.
						*[#0BCFE7] -De control. \n - De direccion de instruccions o de datos. \n -De instrucciones o datos.
@endmindmap
```
# Mapa Conceptual "BASURA ELECTRÓNICA"
```plantuml
@startmindmap
*[#21618C] **BASURA ELECTRÓNICA** 
	*[#0BCFE7] Dispositivos electrónicos o eléctricos \n cuya vida útil ha terminado y son desechados.
		*_ tales como 
			*[#E59866] Computadoras, celulares, \n discos duros, audífonos,\n ratones y teclados,\n consolas de videojuegos,\n proyectores, teléfonos.
				*[#ABEBC6] A este tipo de desechos se denomina Residuos de Aparatos Eléctronicos y Electrónicos (**RAEE**).
	*[#0BCFE7] **Reciclar**
		*[#E59866] Permite obtener los materiales de valor \n y hacer un adecuado tratamiento de los\n desechos.
			*[#ABEBC6] Se pueden extrar muchos materiales \n de valor de los residuos electrónicos.
				*_ como:
					*[#33B9AB] -Estaño. \n -Estaño. \n -Oro.\n Cobre. \n Plata.\n Cobalto.\n Acero.
		*_ Reciclaje Legal.
			*[#ABEBC6] Es un proceso costos, complicado y seguro.
		*_ Reciclaje Ilegal.
			*[#ABEBC6] Es un proceso barato, inseguro, dañino \n para el medio ambiente, dañino \n  para la salud, no se recomienda. 
	*[#0BCFE7] **Residuos de Aparatos \n Eléctronicos y Electrónicos **
		*[#E59866] Este tipo de desechos deben ser tratados adecuadamente al momento de su reciclaje.
			*_ ya que
				*[#ABEBC6] se pueden generar materias primas de ellos: 
					*[#33B9AB] - plasticos. \n - Vidrios. \n - Electrónica. \n - Metales.
		*[#E59866] En México se generan en promedio 300 mil toneladas de residuos electronicos.
	*[#0BCFE7] **Plantas de Reciclaje**	
		*[#E59866] Instalación en donde se procesan \n diferentes materiales, en este caso,\n residuos electrónicos, para poder ser reutilizados.
			*[#ABEBC6] Centros de Reciclaje.
				*[#33B9AB] - E-end. \n - Perú Green Recycling, \n Techement, \n REMSA, \n Servicios Ecológicos
@endmindmap
```